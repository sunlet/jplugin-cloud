package net.jplugin.cloud.common;

public class CloudPluginPriority {
    public final static int BASE = -100;
    public final static int CLIENT = BASE + 3;
    public final static int SERVER = BASE + 2;
    public final static int IO = BASE+1;


}
