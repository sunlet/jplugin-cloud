package net.jplugin.cloud.rpc.common.constant;

public class AppConstants {
	public static final String APP_CODE = "appcode";

	public static final String APP_SERVICES = "services";

	public static final String SERVICE_CODE = "servicecode";

	public static final String SERVICE_OPERATION = "operations";

	public static final String OPERATION_NAME = "operationname";

	public static final String OPERATION_CODE = "operationcode";

	public static final String ANNOTATION_KEY = "annotations";

	public static final String ANNOTATION_NAME = "_name";

	public static final String ANNOTATION_CT = "_ct";

	public static final String ANNOTATION_RL = "_rl";

	// rpc调用appcode
	public static final String _APP_ID = "_aid";
	// rpc调用app的token
	public static final String _APP_TOKEN = "_atk";

	public static final String _OPER_ID = "_oid";

	public static final String _OPER_TOKEN = "_otk";

	public static final String _TENANT_ID = "_tenant_id";

	public static final String _REQUEST_ID = "_request_id";

	public static final String _TRACE_ID = "_trace_id";

	public static String _CLIENT_IP = "_cip";

	public static String _result = "result";
	
	public static String ENV_RETAILO2O="retailo2o";
	public static String ENV_HAIZIWANG="haiziwang";

	public static String SEND_HEART_METHOD ="sendHeart";
	public static String REGISTRY_SERVICE_NAME ="/registryService";
	public static String ESF_REGISTRY="esf-registry";
}
