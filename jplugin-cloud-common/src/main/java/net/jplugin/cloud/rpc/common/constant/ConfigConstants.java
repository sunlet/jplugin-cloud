package net.jplugin.cloud.rpc.common.constant;

public class ConfigConstants {

	public static final String ESF_GROUP = "esf_config_group";

	public static final String ESF_PREFIX = "esf.";

	public static final String REGISTRY_URL = "esf.registry.url";

	public static final String NETTY_BOSS = "netty.boss";

	public static final String NETTY_HTTP_BOSS = "netty.http.boss";

	public static final String NETTY_WORKERS = "netty.worker";

	public static final String NETTY_HTTP_WORKERS = "netty.http.worker";

	public static final String RPC_WORKERS = "rpc.worker";

	public static final String RPC_MAX_WORKERS = "rpc.max.worker";

	public static final String RPC_RESP_WORKERS = "rpc.resp.worker";

	public static final String RPC_RESP_MAX_WORKERS = "rpc.resp.max.worker";

	public static final String HTTP_WORKERS = "http.worker";

	public static final String HTTP_MAX_WORKERS = "http.max.worker";

	public static final String CPP_TRY = "cpp.try";

	public static final String HEART_INTERVAL = "heart.interval";

	public static final String HEART_CONTINUOUS_NUM = "heart.continuous.num";

	public static final String CONN_TIMEOUT = "connection.timeout";

	public static final String SO_TIMEOUT = "so.timeout";

	public static final String KAUTH_PROXY_URL = "kauth.center.url";

	public static final String KAUTH_PROXY_DOMAIN_URL = "kauth.proxy.url";

	public static final String KAUTH_CYCLE = "kauth.cycle";

	public static final String KAUTH_DELAY = "kauth.delay";

	public static final String WAIT_TIMEOUT = "wait.timeout";

	public static final String BLACK_TIMEOUT = "black.timeout";

	public static final String REGISTER_CENTER_SITE = "esf.register.url";

	public static final String REGISTRY_DOMAIN_URL = "esf.site.url";
	public static final String TENANT_DOMAIN_URL = "platform.tenant.host";

	public static final String REGISTRY_SYNC = "registry.sync.old";

	public static final String CPP_MONITOR = "monitor.cpp";

	public static final String CPP_SOURCE_LUCENCY = "lucency.source.cpp";

	public static final String CPP_MACHINEKEY_LUCENCY = "lucency.machinekey.cpp";

	public static final String CPP_TANANCY_OVERRIDE = "tanancy.override.cpp";

	public static final String REGISTRY_STATE_CHECK = "registry.state.check";

	public static final String MOINTOR_REMOTEEXCEP = "monitor.remoteexcep";

	public static final String MOINTOR_CLIENT_REMOTEEXCEP = "monitor.client.remoteexcep";

	public static final String DEBUG_MODE = "debug.mode";

	public static final String DEPLOY_PORT0 = "PORT0";

	public static final String ENABLE_DEPLOY_PORT = "deploy.port.enable";

	public static final String REJECT_ALL = "reject.all";

	public static final String ADD_SERVICE = "service.add";

	public static final String DELETE_SUBSCRIBER = "subscriber.delete";

	public static final String BATCH_QUERY_CACHE_PROXY_MAX_SIZE = "batch.query.cache.proxy.max.size";

	public static final String BATCH_QUERY_DB_MAX_SIZE = "batch.query.db.max.size";

	public static final String CACHE_MODE = "cache.mode";

	public static final String PROVIDER_STATE_CHECK = "provider.state.check";

	public static final String WX_IP_RULE = "wx.ip.rule";

	public static final String UC_IP_RULE = "uc.ip.rule";
	
	public static final String BABY_STORE_MODE="baby.store.mode";

	public static final Integer DEFAULT_PRORITITY=-400;

}
