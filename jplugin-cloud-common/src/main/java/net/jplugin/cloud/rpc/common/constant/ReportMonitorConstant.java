package net.jplugin.cloud.rpc.common.constant;

public class ReportMonitorConstant {

	public static final String SERVICE_RPC_CODE = "rpc";

	public static final String SERVICE_WEB_CODE = "web";

	public static final String SERVICE_RISK_CODE = "risk";

	public static final Integer CODE_SUCCESS = 1;

	public static final Integer CODE_FAILURE = 0;

	public static final String SPLICE_CODE = "/";

	public static final String NULL_CODE = "NULL";

	public static final String FILTER_STARTTIME = "monitor_start_time";

	public static final String FILTER_MONITOR_SERVICE_URL = "monitor_service_url";
}
