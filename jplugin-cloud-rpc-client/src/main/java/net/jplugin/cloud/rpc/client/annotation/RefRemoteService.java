package net.jplugin.cloud.rpc.client.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface RefRemoteService {
	// public String remoteService() default "";
}
